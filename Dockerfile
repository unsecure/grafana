FROM grafana/grafana:8.1.0

RUN cd /var/lib/grafana/plugins && \
    wget https://github.com/vsergeyev/loudml-grafana-app/raw/master/loudml-grafana-app-1.7.2.zip && \
    unzip loudml-grafana-app-1.7.2.zip && \
    rm loudml-grafana-app-1.7.2.zip
